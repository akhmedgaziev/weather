import requests

from .settings import OWM_API_KEY

WEATHER_API_URL = 'https://api.openweathermap.org/data/2.5/weather/'
IMAGE_API_URL = 'http://openweathermap.org/img/w/'

def get_weather(city):
    response = requests.get(WEATHER_API_URL, {'q': city, 'apiKey': OWM_API_KEY})
    data = response.json()
    if data['cod'] == 200:
        return '[{}]({})'.format(data['weather'][0]['main'], 
                                 requests.compat.urljoin(IMAGE_API_URL, data['weather'][0]['icon'] + '.png'))
    else: 
        return data['message']