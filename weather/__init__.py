"""
Telegram bot for weather
"""
import logging
from telegram.ext import Updater, Dispatcher

from .settings import TELEGRAM_API_TOKEN
from .handlers import handlers

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

updater = Updater(token=TELEGRAM_API_TOKEN)

# Create dispatcher
dispatcher = updater.dispatcher

# Register all handlers
for handler in handlers:
    dispatcher.add_handler(handler)
