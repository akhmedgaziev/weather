"""
Settings for project
"""

import os

OWM_API_KEY = os.environ.get('OWM_API_KEY')
TELEGRAM_API_TOKEN = os.environ.get('TELEGRAM_API_TOKEN')
