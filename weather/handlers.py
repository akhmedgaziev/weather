"""
Handlers
"""
from telegram.ext import CommandHandler, MessageHandler, Filters

from .utils import get_weather

def start(bot, update):
    bot.send_message(chat_id=update.message.chat_id, text="Hello, i am weather bot. Send city name to get weather.")

def weather(bot, update):
    weather = get_weather(update.message.text)
    bot.send_message(chat_id=update.message.chat_id, text=weather, parse_mode="Markdown")

start_handler = CommandHandler('start', start)
weather_handler = MessageHandler(Filters.text, weather)

handlers = [
    start_handler,
    weather_handler,
]