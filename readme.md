# Weather
Telegram bot for weather

## Usage

### Prerequisites
Before you are ready to run Wather you will need additional software installed on your computer.

#### Python 3
Weather requires Python 3.5 or later.

### Instalation
- Clone the repository
- Enter the directory
- Install all dependencies:

        $ pip install pipenv && pipenv install

- Start the development server:

        $ pipenv run python start.py